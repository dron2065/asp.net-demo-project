using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NumberSquares.Controllers;
using NumberSquares.Domain;
using NumberSquares.Services;
using NumberSquares.Services.Impl;

namespace NumberSquares.Tests;

public class NumberSquaresControllerTests
{
    [Fact]
    public void InputDataNull_BadRequest()
    {
        var settingsMock = new Mock<ISettings>();
        settingsMock.Setup(x => x.MinValue).Returns(0.0);
        settingsMock.Setup(x => x.MaxValue).Returns(100.0);
        settingsMock.Setup(x => x.NumbersLimit).Returns(10);
        settingsMock.Setup(x => x.MinWaitTime).Returns(500);
        settingsMock.Setup(x => x.MaxWaitTime).Returns(1000);

        var cacherMock = new Mock<ICacher>();
        cacherMock.Setup(x => x.CacheResult(It.IsAny<double>(), It.IsAny<double>()));
        cacherMock.Setup(x => x.GetCachedResult(It.IsAny<double>())).Returns(null as double?);

        var target = new NumberSquaresController(
            new Calculator(settingsMock.Object, cacherMock.Object),
            settingsMock.Object);
        var result = target.Calculate(new CalculationsRequestDto
        {
            Values = null,
        });
        Assert.IsType<BadRequestObjectResult>(result);
        Assert.Equal((result as BadRequestObjectResult).Value, "Values are not filled");
    }

    [Fact]
    public void InputVeryBig_BadRequest()
    {
        var settingsMock = new Mock<ISettings>();
        settingsMock.Setup(x => x.MinValue).Returns(0.0);
        settingsMock.Setup(x => x.MaxValue).Returns(100.0);
        settingsMock.Setup(x => x.NumbersLimit).Returns(2);
        settingsMock.Setup(x => x.MinWaitTime).Returns(500);
        settingsMock.Setup(x => x.MaxWaitTime).Returns(1000);

        var cacherMock = new Mock<ICacher>();
        cacherMock.Setup(x => x.CacheResult(It.IsAny<double>(), It.IsAny<double>()));
        cacherMock.Setup(x => x.GetCachedResult(It.IsAny<double>())).Returns(null as double?);

        var target = new NumberSquaresController(
            new Calculator(settingsMock.Object, cacherMock.Object),
            settingsMock.Object);
        var result = target.Calculate(new CalculationsRequestDto
        {
            Values = new double[] { 1.0, 2.0, 3.0 },
        });
        Assert.IsType<BadRequestObjectResult>(result);
        Assert.Equal((result as BadRequestObjectResult).Value, "Too much values (max 2)");
    }

    [Fact]
    public void NumbersNotInRange_BadRequest()
    {
        var settingsMock = new Mock<ISettings>();
        settingsMock.Setup(x => x.MinValue).Returns(2.0);
        settingsMock.Setup(x => x.MaxValue).Returns(3.0);
        settingsMock.Setup(x => x.NumbersLimit).Returns(10);
        settingsMock.Setup(x => x.MinWaitTime).Returns(500);
        settingsMock.Setup(x => x.MaxWaitTime).Returns(1000);

        var cacherMock = new Mock<ICacher>();
        cacherMock.Setup(x => x.CacheResult(It.IsAny<double>(), It.IsAny<double>()));
        cacherMock.Setup(x => x.GetCachedResult(It.IsAny<double>())).Returns(null as double?);

        var target = new NumberSquaresController(
            new Calculator(settingsMock.Object, cacherMock.Object),
            settingsMock.Object);
        var result = target.Calculate(new CalculationsRequestDto
        {
            Values = new double[] { 1.0, 2.0, 3.0 },
        });
        Assert.IsType<BadRequestObjectResult>(result);
        Assert.Equal((result as BadRequestObjectResult).Value, "Not all numbers at are in allowable range");
    }

    [Fact]
    public void CorrectInput_CorrectOutput()
    {
        var settingsMock = new Mock<ISettings>();
        settingsMock.Setup(x => x.MinValue).Returns(0.0);
        settingsMock.Setup(x => x.MaxValue).Returns(100.0);
        settingsMock.Setup(x => x.NumbersLimit).Returns(10);
        settingsMock.Setup(x => x.MinWaitTime).Returns(500);
        settingsMock.Setup(x => x.MaxWaitTime).Returns(1000);

        var cacherMock = new Mock<ICacher>();
        cacherMock.Setup(x => x.CacheResult(It.IsAny<double>(), It.IsAny<double>()));
        cacherMock.Setup(x => x.GetCachedResult(It.IsAny<double>())).Returns(null as double?);

        var target = new NumberSquaresController(
            new Calculator(settingsMock.Object, cacherMock.Object),
            settingsMock.Object);
        var result = target.Calculate(new CalculationsRequestDto
        {
            Values = new double[] { 1.0, 2.0, 3.0 },
        });
        Assert.IsType<OkObjectResult>(result);
        Assert.IsType<CalculationsResponseDto>((result as OkObjectResult).Value);
        Assert.Equal(
            ((result as OkObjectResult).Value as CalculationsResponseDto).Value,
            14.0);
    }
}
