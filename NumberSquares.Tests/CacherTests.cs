using NumberSquares.Services.Impl;

namespace NumberSquares.Tests;

public class CacherTests
{
    [Fact]
    public void NotCachedValue_ResultNull()
    {
        var target = new Cacher();
        Assert.Equal(null, target.GetCachedResult(15.0));
    }

    [Fact]
    public void CachedValue_ResultMatchToCached()
    {
        var target = new Cacher();
        target.CacheResult(15.0, 225.0);
        Assert.Equal(225.0, target.GetCachedResult(15.0));
    }
}
