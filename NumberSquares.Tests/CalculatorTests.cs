using Moq;
using NumberSquares.Services;
using NumberSquares.Services.Impl;

namespace NumberSquares.Tests;

public class CalculatorTests
{
    [Fact]
    public void InputDataNull_Exception()
    {
        var settingsMock = new Mock<ISettings>();
        settingsMock.Setup(x => x.NumbersLimit).Returns(10);
        settingsMock.Setup(x => x.MinWaitTime).Returns(500);
        settingsMock.Setup(x => x.MaxWaitTime).Returns(1000);

        var cacherMock = new Mock<ICacher>();
        cacherMock.Setup(x => x.CacheResult(It.IsAny<double>(), It.IsAny<double>()));
        cacherMock.Setup(x => x.GetCachedResult(It.IsAny<double>())).Returns(null as double?);

        var target = new Calculator(settingsMock.Object, cacherMock.Object);
        Assert.Throws<ArgumentNullException>(() => target.Calculate(null as double[]));
    }

    [Fact]
    public void CorrectInput_CorrectOutput()
    {
        var settingsMock = new Mock<ISettings>();
        settingsMock.Setup(x => x.NumbersLimit).Returns(10);
        settingsMock.Setup(x => x.MinWaitTime).Returns(500);
        settingsMock.Setup(x => x.MaxWaitTime).Returns(1000);

        var cacherMock = new Mock<ICacher>();
        cacherMock.Setup(x => x.CacheResult(It.IsAny<double>(), It.IsAny<double>()));
        cacherMock.Setup(x => x.GetCachedResult(It.IsAny<double>())).Returns(null as double?);

        var target = new Calculator(settingsMock.Object, cacherMock.Object);
        var result = target.Calculate(new double[] { 1.0, 3.0, 10.0 });
        Assert.Equal(110.0, result);
    }
}
