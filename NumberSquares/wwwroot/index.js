/**
 * Обработка нажатия на кнопку
 */
window.addEventListener("load", (event) => {
    document.getElementById("calculateButton").addEventListener("click", calculate);
});

/**
 * Выполнение запроса, отображение результата и запись в историю
 * @param {*} event 
 * @returns 
 */
async function calculate(event)
{
    var inputString = document.getElementById("numbersInput").value;
    var numbers = inputString.split(" ").filter((slice) => slice !== "").map(slice => Number(slice));
    if (numbers.some(number => isNaN(number)))
    {
        alert("Одно из введённых чисел некорректно");
        return;
    }

    var response = await fetch("/calculate", {
        "method": "POST",
        "cache": "no-cache",
        "credentials": "same-origin",
        "headers": {
            "Content-Type": "application/json",
        },
        "body": JSON.stringify({
            "values": numbers
        })
    });

    var result = (await response.json()).value;
    var resultString = "Результат: " + result;

    document.getElementById("resultField").innerHTML = resultString;

    document.getElementById("historyArea").innerHTML +=
        "<hr>" +
        new Date(Date.now()).toLocaleString() + "<br>" +
        "Исходные данные: " + numbers.join(", ") + "<br>" +
        "Результат: " + result;
}
