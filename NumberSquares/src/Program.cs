using NumberSquares.Services;
using NumberSquares.Services.Impl;

var builder = WebApplication.CreateBuilder(args);

// Добавление сервисов в контейнер

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<ISettings, Settings>();
builder.Services.AddTransient<ICalculator, Calculator>();
builder.Services.AddSingleton<ICacher, Cacher>();

var app = builder.Build();

// Настройка приложения на период разработки
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseStaticFiles();
app.MapGet(
    "/",
    (HttpContext context) => context.Response.Redirect("/index.html"));

app.Run();
