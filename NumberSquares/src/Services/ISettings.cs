namespace NumberSquares.Services;

/// <summary>
/// Интерфейс объекта настроек
/// </summary>
public interface ISettings
{
    /// <summary>
    /// Минимальное значение для числа
    /// </summary>
    /// <value></value>
    double MinValue { get; }

    /// <summary>
    /// Максимальное значение для числа
    /// </summary>
    /// <value></value>
    double MaxValue { get; }

    /// <summary>
    /// Макс. размер входного массива чисел
    /// </summary>
    /// <value></value>
    int NumbersLimit { get; }

    /// <summary>
    /// Минимальное время эмуляции вычисления квадрата одного числа
    /// (в миллисекундах)
    /// </summary>
    /// <value></value>
    int MinWaitTime { get; }

    /// <summary>
    /// Максимальное время эмуляции вычисления квадрата одного числа
    /// (в миллисекундах)
    /// </summary>
    /// <value></value>
    int MaxWaitTime { get; }
}
