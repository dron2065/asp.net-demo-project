using System.Collections.Concurrent;

namespace NumberSquares.Services.Impl;

/// <summary>
/// Кэш результатов вычисления
/// </summary>
public class Cacher : ICacher
{
    /// <summary>
    /// Физичекое хранилище кшированных результатов
    /// </summary>
    /// <returns></returns>
    private ConcurrentDictionary<double, double> _cachedData = new();

    /// <inheritdoc/>
    public void CacheResult(double input, double result)
    {
        _cachedData[input] = result;
    }

    /// <inheritdoc/>
    public double? GetCachedResult(double input)
    {
        return _cachedData.TryGetValue(input, out var cachedValue) ?
            cachedValue :
            null;
    }
}
