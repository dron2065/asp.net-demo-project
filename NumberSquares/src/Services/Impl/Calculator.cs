namespace NumberSquares.Services.Impl;

/// <summary>
/// Реализация вычислительного компонента
/// </summary>
public class Calculator : ICalculator
{
    /// <summary>
    /// Настройки приложения
    /// </summary>
    private readonly ISettings _settings;

    /// <summary>
    /// Сервис кэширования результатов
    /// </summary>
    private readonly ICacher _cacher;

    /// <summary>
    /// Объект генерации случайных чисел для выбора промежутка
    /// </summary>
    /// <returns></returns>
    private readonly Random _random = new Random();

    public Calculator(ISettings settings, ICacher cacher)
    {
        _settings = settings;
        _cacher = cacher;
    }

    /// <inheritdoc/>
    public double Calculate(double[] input)
    {
        ArgumentNullException.ThrowIfNull(input);

        var minWait = _settings.MinWaitTime;
        var maxWait = _settings.MaxWaitTime + 1;

        var result = 0.0;
        for (int i = 0; i < input.Length; i++)
        {
            var cachedValue = _cacher.GetCachedResult(input[i]);
            if (cachedValue.HasValue)
            {
                result += cachedValue.Value;
            }
            else
            {
                Thread.Sleep(_random.Next(minWait, maxWait));
                var square = Math.Pow(input[i], 2);
                _cacher.CacheResult(input[i], square);
                result += square;
            }
        }

        return result;
    }
}
