namespace NumberSquares.Services.Impl;

/// <summary>
/// Настройки приложения
/// </summary>
public class Settings : ISettings
{
    /// <inheritdoc/>
    public double MinValue { get; }

    /// <inheritdoc/>
    public double MaxValue { get; }

    /// <inheritdoc/>
    public int NumbersLimit { get; }

    /// <inheritdoc/>
    public int MinWaitTime { get; }

    /// <inheritdoc/>
    public int MaxWaitTime { get; }

    public Settings(IConfiguration configuration)
    {
        MinValue = configuration.GetValue<double>("MinValue", 10.0);
        MaxValue = configuration.GetValue<double>("MaxValue", 100.0);
        NumbersLimit = configuration.GetValue<int>("NumbersLimit", 10);
        MinWaitTime = configuration.GetValue<int>("MinWaitTime", 500);
        MaxWaitTime = configuration.GetValue<int>("MaxWaitTime", 1000);
    }
}
