namespace NumberSquares.Services;

/// <summary>
/// Интерфейс кэша результатов вычисления
/// </summary>
public interface ICacher
{
    /// <summary>
    /// Сохранить в кэш вычисленный результат
    /// </summary>
    /// <param name="result"></param>
    void CacheResult(double input, double result);

    /// <summary>
    /// Получить из кэша ранее сохранённый результат
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    double? GetCachedResult(double input);
}
