namespace NumberSquares.Services;

/// <summary>
/// Интерфейс вычислительного компонента
/// </summary>
public interface ICalculator
{
    /// <summary>
    /// Выполнить вычисления
    /// </summary>
    /// <param name="input">Входной набор чисел</param>
    /// <returns>Вычисленные квадраты чисел</returns>
    double Calculate(double[] input);
}
