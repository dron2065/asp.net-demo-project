using System.Text.Json.Serialization;

namespace NumberSquares.Domain;

/// <summary>
/// Данные ответа на запрос по проведению вычислений
/// </summary>
public class CalculationsResponseDto
{
    /// <summary>
    /// Вычисленные значения
    /// </summary>
    /// <value></value>
    [JsonPropertyName("value")]
    public double Value { get; set; }
}
