using System.Text.Json.Serialization;

namespace NumberSquares.Domain;

/// <summary>
/// Данные запроса на проведение вычислений
/// </summary>
public class CalculationsRequestDto
{
    /// <summary>
    /// Числа, квадраты которых нужно вычислить
    /// </summary>
    /// <value></value>
    [JsonPropertyName("values")]
    public double[]? Values { get; set; }
}
