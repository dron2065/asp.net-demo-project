using Microsoft.AspNetCore.Mvc;
using NumberSquares.Domain;
using NumberSquares.Services;

namespace NumberSquares.Controllers;

/// <summary>
/// Контроллер, отвечающий за вычисление квадратов чисел
/// </summary>
[ApiController]
[Route("calculate")]
public class NumberSquaresController : ControllerBase
{
    /// <summary>
    /// Настройки приложения
    /// </summary>
    private readonly ISettings _settings;

    /// <summary>
    /// Компонент, выполняющий вычисления
    /// </summary>
    private readonly ICalculator _calculator;

    public NumberSquaresController(ICalculator calculator, ISettings settings)
    {
        _settings = settings;
        _calculator = calculator;
    }

    /// <summary>
    /// Выполнить вычисления по запросу
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult Calculate([FromBody] CalculationsRequestDto request)
    {
        if (request.Values == null)
        {
            return BadRequest("Values are not filled");
        }

        if (request.Values.Length > _settings.NumbersLimit)
        {
            return BadRequest($"Too much values (max {_settings.NumbersLimit})");
        }

        if (!NumbersAreCorrect(request.Values))
        {
            return BadRequest("Not all numbers at are in allowable range");
        }

        return Ok(new CalculationsResponseDto
        {
            Value = _calculator.Calculate(request.Values)
        });
    }

    /// <summary>
    /// Проверить числа на вхождение в диапазон
    /// </summary>
    /// <param name="numbers"></param>
    /// <returns></returns>
    private bool NumbersAreCorrect(double[] numbers)
    {
        foreach (var number in numbers)
        {
            if (number < _settings.MinValue || number > _settings.MaxValue)
            {
                return false;
            }
        }

        return true;
    }
}
